var canvas = document.getElementById("screen");
var gfx    = canvas.getContext("2d");

// Définition du type Piece
var Piece = function(name, color, line, column){
    this.name    = name   || 'empty';
    this.line    = line   || 0;
    this.column  = column || 0;
    this.color   = color  || 'grey';
    this.pieceId = undefined;
};

Piece.prototype.orientation = function() {
    return (this.color === 'white') ? +1 : -1;
}

// Définition du type Pawn
var Pawn = function(color, line, column) {
    Piece.prototype.constructor.call(this, 'Pawn', color, line, column);
    this.pieceId = [[0, 5], [480, 5]];
}
Pawn.prototype = new Piece();

Pawn.prototype.getMoves = function() {
    var arr_moves = [];

    if (isEmpty(this.line + this.orientation(), this.column) &&
		  this.line + this.orientation() < board.length) {
	arr_moves[0] = [this.column, this.line + this.orientation()];
    }
/*	
    if (this.color == "white") {
	if (this.line == 1 &&
	    this.line + this.orientation() * 2 < board.length)
	    arr_moves[1] = [this.column, this.line + this.orientation() * 2];
    } else {
	if (this.line == board.length - 2 &&
	    this.line + this.orientation() * 2 < board.length)
	    arr_moves[1] = [this.column, this.line + this.orientation() * 2];
    }
*/
    return arr_moves;
}

// Définition du type Queen
var Queen = function(color, line, column) {
    Piece.prototype.constructor.call(this, 'Queen', color, line, column);
    this.pieceId = [[320, 5], [802, 5 ]];
}
Queen.prototype = new Piece();

// Définition du type King
var King = function(color, line, column) {
    Piece.prototype.constructor.call(this, 'King', color, line, column);
    this.pieceId = [[400, 5], [880, 5]];
}
King.prototype = new Piece();

// TODO autres pièces

// Créer un plateau avec des pieces vide
var createBoard = function(nbLine, nbColumn) {
    var res = [];
    for (var i = 0; i < nbLine; i++) {
	res[i] = [];
	for (var j = 0; j < nbColumn; j++) {
	    res[i][j] = new Piece("", "", i, j);
	}
    }
    return res;
}

var isEmpty = function(lig, col) {
    return board[lig][col].name == "empty" ? true : false;
}

var put = function(lig, col, piece) {
    board[lig][col] = piece;
}

var board = createBoard(8, 8);

// Créer un plateau avec les pièces du jeu
var initBoard = function() {
    // Pawn
    for (var i = 0; i < board.length; i++) {
	board[1][i] = new Pawn("white", 1, i);
	board[board.length - 2][i] = new Pawn("black", board.length - 2, i);
    }

  //  board[2][3] = new Pawn("white", 2, 3); // temp
    // TODO autres pièces
}

var convertCoordinates = function(ligPixel, colPixel) {
    var lig = Math.ceil(ligPixel / (canvas.height/8)) - 1;
    var col = Math.ceil(colPixel / (canvas.width /8)) - 1;
    return [lig, col];
}

// liste des coordonnées des cellules libres accessibles par la pièce actuellement
// sélectionnée par l'utilisateur
var highlightedCells = [];

// on prend la coordonnée de la cellule à dessiner et une couleur correspondant 
// au carré dessiné dans la case si elle ne contient pas de pièce (gris si la 
// case n'est pas sélectionnée pour représenter un déplacement valide et jaune sinon) 
var drawCell = function(coord, color) {
    var lenx = canvas.width / board[0].length;
    var leny = canvas.height / board.length;

    if (color != "yellow") {
	gfx.fillStyle = color;
	gfx.fillRect(coord[0] * lenx, coord[1] * leny, lenx, leny);
    }
    
    if (!isEmpty(coord[1], coord[0])) {
	var tmp = board[coord[1]][coord[0]];
	if (tmp.color == "white") {
	    gfx.drawImage(chessSymbols, tmp.pieceId[0][0], tmp.pieceId[0][1],
			  75, 75, coord[0] * lenx, coord[1] * leny, 50, 50);
	} else {
	    gfx.drawImage(chessSymbols, tmp.pieceId[1][0], tmp.pieceId[1][1],
			  75, 75, coord[0] * lenx, coord[1] * leny, 50, 50);
	}
    } else {
	if (highlightedCells.includes(coord)) {
	    gfx.strokeStyle = color;
	} else {
	    gfx.strokeStyle = "rgb(100,100,100)";
	}
	gfx.strokeRect((coord[0] * lenx) + 5, (coord[1] * leny) + 5,
			   lenx - 10, leny - 10);
    }
}

var highlight = function(on) {
    if (on == true) {
	for (var i = 0; i < highlightedCells.length; i++) {
	    drawCell(highlightedCells[i], "yellow");
	}
    } else {
	drawGrid();
    }
}

canvas.addEventListener("mousedown", mouseClicked, false);
function mouseClicked(event) {
    var ligCoord = event.pageY - canvas.offsetTop;
    var colCoord = event.pageX - canvas.offsetLeft;
    var coord    = convertCoordinates(ligCoord, colCoord);
    if (highlightedCells.length > 0) {
        highlight(false);
        highlightedCells = [];
    }
    var piece = board[coord[0]][coord[1]];
    console.log("i=" + coord[0] + ", j=" + coord[1] + ", piece=" + piece.name);
    if (piece.name !== 'empty') {
	highlightedCells = piece.getMoves();
        highlight(true);
    } else {
        // TODO: if empty and highlighted, move the piece !
        // doMove();
    }
}

// initialise le plateau en déposant les pièces de deux joueurs au début de la partie
initBoard();

// Pour dessiner le plateau, on spécifie le coin supérieur gauche, la 
// largeur et la hauteur. Dans cette fonction, on appelle drawCell 
// pour dessiner une cellule à une coordonnée donnée.
var drawGrid = function(x, y, width, height, nbLig, nbCol) {
    // Dark Grey
    for (var i = 0; i < board.length; i += 2) {
	for (var j = 0; j < board[i].length; j += 2) {
	    drawCell([i, j], "rgb(121,121,121)");
	}
	
	for (var j = 1; j < board[i + 1].length; j += 2) {
	    drawCell([i + 1, j], "rgb(121,121,121)");
	}	
    }

    // Light Grey
    for (var i = 0; i < board.length; i += 2) {
	for (var j = 1; j < board[i + 1].length; j += 2) {
	    drawCell([i, j], "rgb(169,169,169)");
	}

	for (var j = 0; j < board[i].length; j += 2) {
	    drawCell([i + 1, j], "rgb(169,169,169)");
	}
    }
}
